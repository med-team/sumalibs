PREFIX=/usr/local

SOURCES = libfasta/fasta_header_parser.c \
		  libfasta/fasta_seq_writer.c \
		  libfasta/fasta_header_handler.c \
		  libfasta/header_mem_handler.c \
          libfasta/sequence.c \
          libfile/fileHandling.c \
          liblcs/sse_banded_LCS_alignment.c \
		  liblcs/upperband.c \
		  libutils/utilities.c \
          libutils/debug.c  
                   
SRCS=$(SOURCES)
         
OBJECTS= $(patsubst %.c,%.o,$(SOURCES))

LIBFILE = libsuma.a

RANLIB = ranlib

CC=gcc

LDFLAGS=

CFLAGS = -O3 -w

default: all

all: $(LIBFILE)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $< $(LIB)

libfasta/fasta_header_parser.c: libfasta/fasta_header_parser.l
	flex -Pheader_yy -t $< > $@

libfasta/dic_parser.c: libfasta/dic_parser.l
	lex -Phashtable_yy -t $< > $@

clean:
	rm -rf $(OBJECTS) $(LIBFILE)

$(LIBFILE): $(OBJECTS)
	ar -cr $@ $?
	$(RANLIB) $@

install: all
	install -d $(DESTDIR)$(PREFIX)/lib/
	install -m 644 $(LIBFILE) $(DESTDIR)$(PREFIX)/lib/
	install -d $(DESTDIR)$(PREFIX)/include/
	for lib in libfasta liblcs libsse libutils libfile ; do \
		install -d $(DESTDIR)$(PREFIX)/include/$$lib ; \
		cp $$lib/*.h $(DESTDIR)$(PREFIX)/include/$$lib ; \
	done

